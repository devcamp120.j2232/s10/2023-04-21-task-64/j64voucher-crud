package com.devcamp.j64crudapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J64crudApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(J64crudApiApplication.class, args);
	}

}
